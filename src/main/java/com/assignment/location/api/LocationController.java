package com.assignment.location.api;

import com.assignment.location.service.ElasticSearchQuery;
import com.assignment.location.model.LocationModel;
import com.assignment.location.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RequestMapping("api/v1/location")
@RestController
public class LocationController {
    @Autowired
    private ElasticSearchQuery elasticSearchQuery;
    @Autowired
    private  LocationService locationService;


    @PostMapping
    public ResponseEntity<Object> addSearchString(@RequestBody LocationModel locationModel)throws IOException {
        System.out.println(locationModel);
        String response = elasticSearchQuery.createOrUpdateDocument(locationModel);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping
    public Object SearchLocationString(@RequestParam String locationString)throws IOException {
        Object loc= locationService.LocationObject(locationString);
        System.out.println(locationString);

        return loc;

    }
}
