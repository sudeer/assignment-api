package com.assignment.location;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class LocationApplication {


	public static void main(String[] args) {
		SpringApplication.run(LocationApplication.class, args);
	}



}
